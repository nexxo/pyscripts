#!/usr/bin/python3

import os
import sys
import pip

try:
    from pip import main as pipmain
except ImportError:
    from pip._internal import main as pipmain


def lib_install(lib):
    print('There is missing required lib.', lib)
    answer = input('Do you wish to install it using pip (y/n)? ')

    if answer == 'y':
        pipmain(['install', lib])
    else:
        print('Exiting...')
        sys.exit()


# handle imports
try:
    from bs4 import BeautifulSoup
except ImportError:
    lib_install('beautifulsoup4')
try:
    import html5lib
except ImportError:
    lib_install('html5lib')
try:
    import requests
except ImportError:
    lib_install('requests')

# global file tracker
count = 1
THIS_DIR = sys.path[0]
STORAGE = THIS_DIR + '/storage'


def create_storage_dir(where):
    if not os.path.exists(where):
        os.makedirs(where)


def download(url, where):
    global count

    url_split = url.split('.')
    suffix = url_split[len(url_split) - 1]

    file_name = 'pic' + str(count) + '.' + suffix
    response = requests.get(url)
    destination = os.path.join(where, file_name)

    if response.status_code == 200:
        with open(destination, 'wb') as resource:
            resource.write(response.content)
        count += 1
    else:
        print('Failed to download from URL:', url)


def crawl(storage):
    response = requests.get('https://exponea.com/')
    source_code = BeautifulSoup(response.text, features="html5lib")

    # crawl over response text and gather sources
    img_elements = source_code.findAll("img")

    for img_elem in img_elements:
        print('Downloading img from :', img_elem['src'])
        try:
            download(img_elem['src'], storage)
        except Exception:
            print('Skipping bad source url')
            continue
